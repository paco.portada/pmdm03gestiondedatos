package com.iesportada.semipresencial.pmdm03gestiondedatos.ActivityTwo.Settings;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.preference.SwitchPreference;

import com.iesportada.semipresencial.pmdm03gestiondedatos.ActivityTwo.MainActivity2;
import com.iesportada.semipresencial.pmdm03gestiondedatos.ActivityTwo.Sandbox.Utils;
import com.iesportada.semipresencial.pmdm03gestiondedatos.R;


public class SettingsFragment2 extends PreferenceFragment {

    Utils utils = new Utils();
    public static final String PREF_SOUND_SETTINGS = "switch_preference_sound";
    public static final String PREF_FILE_NAME = "edit_text_preference_file_name";
    private EditTextPreference preferenceAlarmFileName;
    private SwitchPreference preferenceSound;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.OnSharedPreferenceChangeListener preferenceChangeListener;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String settings = getArguments().getString("settings2");
        if (settings.equals("general")){
            addPreferencesFromResource(R.xml.general_settings_preferences2);
        }
        else if(settings.equals("about")){
            addPreferencesFromResource(R.xml.help_app_preferences);
        }

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        preferenceAlarmFileName = (EditTextPreference) getPreferenceScreen().findPreference(PREF_FILE_NAME);
        preferenceSound = (SwitchPreference) getPreferenceScreen().findPreference(PREF_SOUND_SETTINGS);

        PreferenceManager.setDefaultValues(getActivity(), R.xml.general_settings_preferences2, false);

        preferenceChangeListener = (sharedPreferences, key)->{

            if(key.equals(PREF_FILE_NAME)){
                Utils.setAlarmName(sharedPreferences.getString(PREF_FILE_NAME, "0"));
                preferenceAlarmFileName.setDefaultValue(sharedPreferences.getString(PREF_FILE_NAME, "0"));
                utils.writeAlarmsOnSD(Utils.recoverAlarmsFromSD());
            }
            if(key.equals(PREF_SOUND_SETTINGS)){
                MainActivity2.setSoundOn(sharedPreferences.getBoolean(PREF_SOUND_SETTINGS, true));
                preferenceSound.setDefaultValue(sharedPreferences.getBoolean(PREF_SOUND_SETTINGS, true));
            }
        };

    }

    @Override
    public void onResume() {
        super.onResume();
        getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(preferenceChangeListener);
    }

    @Override
    public void onPause() {
        super.onPause();
        getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(preferenceChangeListener);
    }

}