package com.iesportada.semipresencial.pmdm03gestiondedatos.ActivityTwo.Sandbox;

public class Constants {
    public static final int MAX_ALARMS = 5;
    public static final int REQUEST_WRITE = 1;
}
