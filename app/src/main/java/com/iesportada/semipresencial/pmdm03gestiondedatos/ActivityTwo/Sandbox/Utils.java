package com.iesportada.semipresencial.pmdm03gestiondedatos.ActivityTwo.Sandbox;


import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Environment;
import android.widget.Toast;

import com.iesportada.semipresencial.pmdm03gestiondedatos.ActivityTwo.Alarm;
import com.iesportada.semipresencial.pmdm03gestiondedatos.ActivityTwo.MainActivity2;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;

import java.util.ArrayList;
import java.util.Scanner;

public class Utils {

    private static String alarmName;

    public static String getAlarmName() {
        return alarmName;
    }

    public static void setAlarmName(String alarmName) {
        Utils.alarmName = alarmName;
    }

    //Recogemos por parámetro una colección (ArrayList) y la escribimos en la SD

    public void writeAlarmsOnSD(ArrayList listAlarms){

        PrintWriter printWriter = null;
        ArrayList<Alarm> mStoredAlarms = listAlarms;
        //System.out.println(mStoredAlarms);
        try {
            printWriter = new PrintWriter(new FileOutputStream(alarmsFile()));

            //Vamos a recorrer el arrayList que le hemos pasado para guardar tantas alarmas como el usuario haya decidido, entre 0 y 5.

            for(int i =0; i<listAlarms.size(); i++){

                //Para facilitar la lectura, la variable time, que es un Long, la escribimos como un string y separamos todo por comas.

                printWriter.println(mStoredAlarms.get(i).getTime().toString() +
                        "," + mStoredAlarms.get(i).getDescription() +
                        "," + mStoredAlarms.get(i).getSound()
                );
            }
        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());

        } catch (IOException e){
            System.out.println(e.getMessage());
        }finally {
            //Cerramos el escritor.
            if (printWriter != null){
                printWriter.close();
                System.out.println("fichero creado");
                Utils.showMessage("fichero de alarmas creado", MainActivity2.getContext());
            }
        }
    }

    //Método estático para mostrar mensajes en el MainActivity del ejercicio 2. Recuperamos el contexto en esta actividad y la usamos para mostrar los mensajes en ese contexto.

    public static void showMessage(String s, Context context){
        Toast.makeText(context, s, Toast.LENGTH_SHORT).show();
    }


    //Creador del fichero donde se almacena la colección de alarmas

    public static File alarmsFile(){
        //Localizamos la SD externa

        File sdCard = Environment.getExternalStorageDirectory();

        //Damos nombre al fichero

            //setAlarmName("Alarma.txt");

        @SuppressLint("DefaultLocale") String fileName = String.format(getAlarmName());

        //Localizamos el fichero en una carpeta concreta de la SD externa.

        File dir = new File(sdCard.getAbsolutePath() + "/EJAlarmas");

        //Creamos la carpeta que alojará el fichero
        dir.mkdirs();

        //Creamos el fichero con el nombre y ruta dada.
        File myTxtFile = new File(dir, fileName);
        return myTxtFile;
    }


    //Método que nos va a devolver una colección con las alarmas leidas del fichero.

    public static ArrayList<Alarm> recoverAlarmsFromSD(){
        ArrayList<Alarm> recoveredAlarms = new ArrayList<>();
        Scanner sc = null;
        BufferedReader reader = null;
        try {
            if(!alarmsFile().exists()){
                alarmsFile();
            }
            // Limpiamos la colección en el caso de que estuviera llena

            sc = new Scanner((alarmsFile()));
            if(!recoveredAlarms.isEmpty()){
                recoveredAlarms.clear();
            }

            //Procedemos a leer linea por linea en un bucle.


            while (sc.hasNextLine()){

                //Creamos un array de Strings, de esta forma, cada linea nos va a devolver un array de Strings con 3 valores. En la posición 0 estará el tiempo, en la 1, la descripción y en la 2 el sonido.

                String[] values = sc.nextLine().split(",");

                //Procedemos a añadir al arrayList una nueva alarma pasándo por parámetros cada uno de los valores almacenados en el array.

                recoveredAlarms.add(new Alarm(Long.valueOf(values[0]), values[1], values[2]));
            }
        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
        } catch (IOException e) {
            System.out.println(e.getMessage());;
        } finally {
            if (reader != null){
                try {
                    reader.close();
                } catch (IOException e) {
                    System.out.println(e.getMessage());
                }
            }
        }
        return recoveredAlarms;
    }

}
