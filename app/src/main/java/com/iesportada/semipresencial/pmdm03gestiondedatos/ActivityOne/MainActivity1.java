package com.iesportada.semipresencial.pmdm03gestiondedatos.ActivityOne;

/**
 * @author Rafa Narvaiza
 * Conversor con toolbar y menú de preferencias en el cuál podremos elegir el tipo de cambio y el fondo de pantalla. Añadimos un fragment extra para mostrar la info del creador de la app.
 *
 * MainActivity1 incluye los métodos de conversión, los manejadores de preferencias para recargarlas en el inicio y los manejadores de colores y cambio.
 */

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;
import com.iesportada.semipresencial.pmdm03gestiondedatos.ActivityOne.Settings.*;
import com.iesportada.semipresencial.pmdm03gestiondedatos.R;
import com.iesportada.semipresencial.pmdm03gestiondedatos.databinding.ActivityMain1Binding;

public class MainActivity1 extends AppCompatActivity {

    private static final String PREF_COLOR_SETTINGS = "list_preference_color_choose" ;
    public static final String PREF_CURRENCY_VALUE = "edit_text_preference_dolar_value";
    private ActivityMain1Binding binding;
    private static double rate;
    private double dolarAmount;
    private double euroAmount;
    static Integer backgroundColor;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        binding = ActivityMain1Binding.inflate(getLayoutInflater());//Inicializamos el binding.

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main1);
        getSupportActionBar().setTitle("Conversor"); //Editamos el títutlo del action bar original.
        View view = binding.getRoot();
        setContentView(view);

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this); //Cargamos las preferencias guardadas.
        PreferenceManager.setDefaultValues(this, R.xml.general_settings_preferences, true);

        if(preferences.getString(PREF_CURRENCY_VALUE, "").equalsIgnoreCase("")){
            setRate(1);
        }
        else{
            setRate(Double.parseDouble(preferences.getString(PREF_CURRENCY_VALUE, ""))); //Seteamos el valor del dolar al último valor por defecto guardado.
        }


        changeBackgroundColor(SettingsFragment.onColorChanged(savedColor())); //Pasamos el color de fondo guardado.
        setBackgroundColor(view);//Seteamos este color de fondo.
    }


    @Override public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (item.getItemId()){
            case R.id.action_settings:
                startActivity(new Intent(this, SettingsActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    protected void onResume(){
        super.onResume();
        View view = binding.getRoot();
        setBackgroundColor(view);
    }


    public String savedColor(){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        String color = preferences.getString(PREF_COLOR_SETTINGS, "0");
        return color;
    }

    public void onDolarClick(View view){

        binding.editTextDolar.setInputType(InputType.TYPE_CLASS_NUMBER |
                InputType.TYPE_NUMBER_FLAG_DECIMAL |
                InputType.TYPE_NUMBER_FLAG_SIGNED);

        binding.editTextDolar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (binding.editTextDolar.getText().toString().equals("NaN") || binding.editTextDolar.getText().toString().equals("")){
                    showToast("Please input a number");
                    binding.editTextDolar.setText("0");
                }else {
                    if(Double.valueOf(binding.editTextDolar.getText().toString()) != 0){
                        try{
                            if (binding.editTextDolar.hasFocus()){
                                setDolarAmount(Double.valueOf(binding.editTextDolar.getText().toString()));
                                binding.editTextEuro.setText(fromDolarToEuro());
                            }
                        } catch (NumberFormatException nfe){
                            showToast(nfe.getMessage());
                        }
                    }
                    else{
                        showToast("Introduzca un valor distinto de 0");
                    }
                }


            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    public void onEuroClick(View view){
        binding.editTextEuro.setInputType(InputType.TYPE_CLASS_NUMBER |
                InputType.TYPE_NUMBER_FLAG_DECIMAL |
                InputType.TYPE_NUMBER_FLAG_SIGNED); //Tratamos el input para evitar cualquier caracter que no sea digito.

        binding.editTextEuro.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (binding.editTextEuro.getText().toString().equalsIgnoreCase("NaN") || binding.editTextEuro.getText().toString().isEmpty()){
                    showToast("Please input a number");
                    binding.editTextEuro.setText("0");
                }else {
                    if(Double.valueOf(binding.editTextEuro.getText().toString()) != 0){ //Evitamos el valor 0 para que no nos de un resultado infinito
                        try{
                            if (binding.editTextEuro.hasFocus()){
                                setEuroAmount(Double.valueOf(binding.editTextEuro.getText().toString()));
                                binding.editTextDolar.setText(fromEuroToDolar());
                            }
                        }catch (NumberFormatException nfe){//Sacamos excepción por si en algún momento se setease el editText algún mensaje con caracteres diferentes de los numéricos.
                            showToast(nfe.getMessage());
                        }
                    }else
                    {
                        showToast("Introduzca un valor distinto de 0");
                    }
                }

            }
            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    public String fromDolarToEuro(){
        String result = Double.toString((getRate())*(Double.valueOf(getDolarAmount())));

        return result;
    }

    public String fromEuroToDolar(){
        String result = Double.toString(((Double.valueOf(getEuroAmount()))/getRate()));
        return result;
    }

    private void showToast(String s){
        Toast.makeText(this, s, Toast.LENGTH_LONG).show();
    }

    public static void changeBackgroundColor(Integer color){
        backgroundColor = color;
    }

    public static void setBackgroundColor(View v){
        if (backgroundColor != null){
            v.setBackgroundColor(backgroundColor);

        }
    }

    public static double getRate() {
        return rate;
    }

    public static void setRate(double rate) {
        MainActivity1.rate = rate;
    }

    public double getDolarAmount() {
        return dolarAmount;
    }

    public void setDolarAmount(double dolarAmount) {
        this.dolarAmount = dolarAmount;
    }

    public double getEuroAmount() {
        return euroAmount;
    }

    public void setEuroAmount(double euroAmount) {
        this.euroAmount = euroAmount;
    }
}