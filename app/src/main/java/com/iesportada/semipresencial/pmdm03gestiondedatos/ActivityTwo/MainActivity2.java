package com.iesportada.semipresencial.pmdm03gestiondedatos.ActivityTwo;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.CountDownTimer;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import com.iesportada.semipresencial.pmdm03gestiondedatos.ActivityTwo.List.CustomListActivity;
import com.iesportada.semipresencial.pmdm03gestiondedatos.ActivityTwo.Sandbox.Constants;
import com.iesportada.semipresencial.pmdm03gestiondedatos.ActivityTwo.Sandbox.Utils;
import com.iesportada.semipresencial.pmdm03gestiondedatos.ActivityTwo.Settings.SettingsActivity;
import com.iesportada.semipresencial.pmdm03gestiondedatos.R;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Locale;

public class MainActivity2 extends AppCompatActivity {



    public static final String PREF_SOUND_SETTINGS = "switch_preference_sound";
    public static final String PREF_FILE_NAME = "edit_text_preference_file_name";

    Utils utils = new Utils();

    private EditText mEditTextStoredAlarmMainTime;
    private EditText mEditTextStoredAlarmDescription;

    private TextView mTextViewCountDown;
    private TextView mTextViewStoredAlarmMainSound;

    private Button mButtonStoredAlarmMain;
    private Button mButtonStartPause;
    private Button mButtonReset;
    private Button mButtonEdit;

    private CountDownTimer mCountDownTimer;

    private boolean mTimerRunning;
    private static boolean soundOn;

    private static Context context;
    private long mStartTimeInMillis;
    private long mTimeLeftInMillis;
    private long mEndTime;

    private int count;
    public int alarmCounter = 0;

    private static ArrayList<Alarm> mStoredAlarms;

    private Alarm alarm;


    public static ArrayList<Alarm> getmStoredAlarms() {
        return mStoredAlarms;
    }

    public static void setmStoredAlarms(ArrayList<Alarm> mStoredAlarms) {
        MainActivity2.mStoredAlarms = mStoredAlarms;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maintwoactivity);
        setContext(this);
        getSupportActionBar().setTitle("Cuenta atrás");


        mEditTextStoredAlarmMainTime = findViewById(R.id.mEditTextStoredAlarmMainTime);
        mEditTextStoredAlarmDescription = findViewById(R.id.mEditTextStoredAlarmMainDescription);
        mTextViewStoredAlarmMainSound = findViewById(R.id.mTextViewStoredAlarmMainSound);
        mTextViewCountDown = findViewById(R.id.text_view_countdown);
        mButtonStartPause = findViewById(R.id.button_start_pause);
        mButtonStoredAlarmMain = findViewById(R.id.mButtonStoredAlarmMain);
        mButtonReset = findViewById(R.id.button_reset);
        mButtonEdit = findViewById(R.id.edit_alarms);

        //Gestionamos preferencias.
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        PreferenceManager.setDefaultValues(this, R.xml.general_settings_preferences2, true);

        if(preferences.getString(PREF_FILE_NAME, "").isEmpty()){
            Utils.setAlarmName("Alarma.txt");
        }
        else{
            Utils.setAlarmName(preferences.getString(PREF_FILE_NAME, ""));
        }
        setSoundOn(preferences.getBoolean(PREF_SOUND_SETTINGS, true));


        setmStoredAlarms(new ArrayList<Alarm>());
        setCount(0);

        mButtonEdit.setOnClickListener(view -> {
            Intent intent = new Intent(MainActivity2.this, CustomListActivity.class);
            startActivity(intent);
        });

        mButtonStoredAlarmMain.setOnClickListener(view -> {
            storeAlarmOnCollection();
            emptyTextViews();

        });

        mButtonStoredAlarmMain.setOnLongClickListener(v -> {
                    save();
                    return false;
                }
        );

        mButtonStartPause.setOnClickListener(v-> {
            hideInputFields();
            if (mTimerRunning) {
                pauseTimer();
            } else {
                startTimer();
            }
        });

        mButtonReset.setOnClickListener(v -> {
            resetTimer();
            showInputFields();
        });

    }

    private void hideInputFields() {
        mEditTextStoredAlarmMainTime.setVisibility(View.INVISIBLE);
        mEditTextStoredAlarmDescription.setVisibility(View.INVISIBLE);
        mTextViewStoredAlarmMainSound.setVisibility(View.INVISIBLE);
        mButtonStoredAlarmMain.setVisibility(View.INVISIBLE);
    }

    private void showInputFields(){
        mEditTextStoredAlarmMainTime.setVisibility(View.VISIBLE);
        mEditTextStoredAlarmDescription.setVisibility(View.VISIBLE);
        mTextViewStoredAlarmMainSound.setVisibility(View.VISIBLE);
        mButtonStoredAlarmMain.setVisibility(View.VISIBLE);
    }


    @Override public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main2, menu);
        return true;
    }

    @Override public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (item.getItemId()){
            case R.id.action_settings2:
                startActivity(new Intent(this, SettingsActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    private void storeAlarmOnCollection(){

        alarm = new Alarm();
        if(mStoredAlarms.size()<Constants.MAX_ALARMS){  //Limitamos el tamaño de la coleccion

            String inputTime = mEditTextStoredAlarmMainTime.getText().toString();
            if(inputTime.length()==0){ // Evitamos la entrada de valor 0.

                utils.showMessage("Introduzca un tiempo", this);
                return; //liberamos el evento del click.
            }

            long millisInput = Long.parseLong(inputTime) * 60000; //pasamos a minutos
            System.out.println("@@@@@@@@@@@@@@@@@@" + millisInput);
            if(millisInput == 0){
                utils.showMessage("Introduzca un numero positivo", this);
                return;
            }
            alarm.setTime(millisInput);

            String inputDescription = mEditTextStoredAlarmDescription.getText().toString();
            alarm.setDescription(inputDescription);

            String sound = mTextViewStoredAlarmMainSound.getText().toString();
            alarm.setSound(sound);

            mStoredAlarms.add(alarm); //añadimos la alarma.

            if(getCount()<=Constants.MAX_ALARMS){
                //showMessage("Alarma número " + getCount() + " de " + millisInput/60000 + " minutos añadida.");
                utils.showMessage(mStoredAlarms.get(getCount()).toString(), this);
                setCount(getCount() + 1);
            }
            if(getCount()>=Constants.MAX_ALARMS){
                mButtonStoredAlarmMain.setText("save");
            }
            if(getCount()>=Constants.MAX_ALARMS+1){ //SETEAR EL COUNTER DESDE FUERA. CUANDO PRESIONES START GETEA EL COUNTER Y OCULTA LOS CAMPOS DE ENTRADA.
                mButtonStoredAlarmMain.setVisibility(View.INVISIBLE);

            }

        }
        else{

            utils.showMessage("Alarmas máximas alcanzadas", this);
            save();
        }

    }


    public void save() {

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        String permissions = Manifest.permission.WRITE_EXTERNAL_STORAGE;

        if (ActivityCompat.checkSelfPermission(this, permissions) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{permissions}, Constants.REQUEST_WRITE);
            Toast.makeText(this, "Por favor, haga pulsación larga sobre ADD de nuevo", Toast.LENGTH_LONG).show();

        } else {
            utils.writeAlarmsOnSD(mStoredAlarms);
        }
    }



    private void emptyTextViews(){
        mEditTextStoredAlarmMainTime.setText("");
        mEditTextStoredAlarmDescription.setText("");
    }



    private void setTime(long milliseconds){
        mStartTimeInMillis=milliseconds;
        resetTimer();
    }

    //@@@@@@@ QUÉ FALTA? Poner las alarmas en loop hasta completar todas, poner sonido y mutear si las
    //@@@@@@@ preferences así lo requieren.

    private void startTimer() {

        if(Utils.alarmsFile().exists() && !Utils.recoverAlarmsFromSD().isEmpty()){
            //mTimeLeftInMillis = Utils.recoverAlarmsFromSD().get(alarmCounter).getTime();
            mEndTime = System.currentTimeMillis() + mTimeLeftInMillis;
            System.out.println("@@@@@@@@@@@@@@@mendtime vale " + mTimeLeftInMillis);
            mCountDownTimer = new CountDownTimer(mTimeLeftInMillis, 1000) {
                @Override
                public void onTick(long millisUntilFinished) {
                    mTimeLeftInMillis = millisUntilFinished;
                    updateCountDownText();
                }
                @Override
                public void onFinish() {
                    mTimerRunning = false;
                    updateWatchInterface();
                    if(alarmCounter<Utils.recoverAlarmsFromSD().size()){
                        alarmCounter = alarmCounter+1;
                    }
                    if (alarmCounter>=Utils.recoverAlarmsFromSD().size()){
                        Utils.showMessage("Ha llegado a la última alarma", MainActivity2.getContext());
                    }

                }
            }.start();
            mTimerRunning = true;
            updateWatchInterface();
        }

    }
    private void pauseTimer() {
        mCountDownTimer.cancel();
        mTimerRunning = false;
        updateWatchInterface();
    }
    private void resetTimer() {
        if(Utils.alarmsFile().exists() && !Utils.recoverAlarmsFromSD().isEmpty()){
            mTimeLeftInMillis = Utils.recoverAlarmsFromSD().get(alarmCounter).getTime(); //En el oncreate hay que llamar a un método que carge las alarmas del fichero si este existe y en el caso de que no exista, que setee el textView de la cuenta atrás en 00.00
            updateCountDownText();
            mButtonReset.setVisibility(View.VISIBLE);
            //updateWatchInterface();
        }

    }
    private void updateCountDownText() {
        int hours = (int) (mTimeLeftInMillis / 1000) / 3600;
        int minutes = (int) ((mTimeLeftInMillis / 1000) %3600) / 60;
        int seconds = (int) (mTimeLeftInMillis / 1000) % 60;

        String timeLeftFormatted;
        if(hours > 0 ){
            timeLeftFormatted = String.format(Locale.getDefault(),
                    "%02d:%02d:%02d", hours, minutes, seconds);
        }
        else{
            timeLeftFormatted = String.format(Locale.getDefault(),
                    "%02d:%02d", minutes, seconds);
        }
        mTextViewCountDown.setText(timeLeftFormatted);
    }

    private void updateWatchInterface() {
        if (mTimerRunning && mButtonStartPause.getText().toString().equalsIgnoreCase("Start")) {
            // mEditTextInput.setVisibility(View.INVISIBLE);
            // mButtonSet.setVisibility(View.INVISIBLE);
            mButtonReset.setVisibility(View.INVISIBLE);
            mButtonStartPause.setText("Pause");
        } else {
            mButtonReset.setVisibility(View.VISIBLE);
            //mEditTextInput.setVisibility(View.VISIBLE);
            // mButtonSet.setVisibility(View.VISIBLE);
            mButtonStartPause.setText("Start");
            if (mTimeLeftInMillis < 1000) {
               // mButtonStartPause.setVisibility(View.INVISIBLE);
            } else {
                mButtonStartPause.setVisibility(View.VISIBLE);
            }
            if (mTimeLeftInMillis < mStartTimeInMillis) {
                mButtonReset.setVisibility(View.VISIBLE);
            } else {
                //mButtonReset.setVisibility(View.INVISIBLE);
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        SharedPreferences prefs = getSharedPreferences("prefs", MODE_PRIVATE);
        mTimeLeftInMillis = prefs.getLong("millisLeft", mStartTimeInMillis);
        mTimerRunning = prefs.getBoolean("timerRunning", false);
        mStartTimeInMillis = prefs.getLong("StartTimeInMillis", 30000);//Cambia esto para que se setee después de rellenar el arrayList
        updateCountDownText();
        updateWatchInterface();

        if(mTimerRunning){
            mEndTime = prefs.getLong("endTime", 0);
            mTimeLeftInMillis = mEndTime - System.currentTimeMillis();

            if(mTimeLeftInMillis < 0){
                mTimeLeftInMillis = 0;
                mTimerRunning = false;
                updateWatchInterface();
                updateCountDownText();
            }else{
                startTimer();
            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        SharedPreferences prefs = getSharedPreferences("prefs", MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();

        editor.putLong("StartTimeInMillis", mStartTimeInMillis);
        editor.putLong("millisLeft", mTimeLeftInMillis);
        editor.putBoolean("timerRunning", mTimerRunning);
        editor.putLong("endTime", mEndTime);
        editor.apply();

        if(mCountDownTimer!= null){
            mCountDownTimer.cancel();
        }
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public static Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public static boolean isSoundOn() {
        return soundOn;
    }

    public static void setSoundOn(boolean soundOn) {
        MainActivity2.soundOn = soundOn;
    }

}
